﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using HW4_ListViews.ClassModels;
using Xamarin.Forms;

namespace HW4_ListViews
{
    public partial class HW4_ListViewsPage : ContentPage
    {

        public HW4_ListViewsPage()
        {
            InitializeComponent();

            PopViewList();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            PopViewList();
            WebSiteLink.IsRefreshing = false;
        }


        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var ListView = (ListView)sender;
            WebsiteItem itemTapped = (WebsiteItem)ListView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }

        private void PopViewList()
        {

            var SiteList = new ObservableCollection<WebsiteItem>()
            {

                new WebsiteItem()
                {
                    IconSource = ImageSource.FromFile("London.png"),
                    websiteName = "1. London.",
                    websiteDescription="The worlds famous city. ",
                    url = "https://www.lonelyplanet.com/england/london",
                },


                new WebsiteItem()
                {
                    websiteName = "2. New York.",
                    IconSource = ImageSource.FromFile("New York.png"),
                    websiteDescription="The most visited city in the US",
                    url = "https://www.lonelyplanet.com/usa/new-york-city",
                },


                new WebsiteItem()
                {
                    websiteName = "3. Paris",
                    IconSource = ImageSource.FromFile("Paris.png"),
                    websiteDescription = "The best tourist destinations for romance.",
                    url = "http://www.travelandleisure.com/travel-guide/ile-de-france-paris",
                },


                new WebsiteItem()
                {
                    websiteName = "4. San Diego",
                    IconSource = ImageSource.FromFile("San Diego.png"),
                    websiteDescription = "The best place, pleasent climates. ",
                    url = "https://www.sandiego.org",

                },

                new WebsiteItem()
                {
                    websiteName = "5. Abu Dhabi",
                    IconSource = ImageSource.FromFile("Abu Dhabi.png"),
                    websiteDescription = "Futuristic magnificiant city.",
                    url = "https://visitabudhabi.ae/us-en/default.aspx",
                },

                new WebsiteItem()
                {
                   websiteName = "6. Singapore",
                    IconSource = ImageSource.FromFile("Singapore.png"),
                    websiteDescription = "The worlds cleanest city.",
                    url = "https://wikitravel.org/en/Singapore",
                },


                new WebsiteItem()
                {
                   websiteName = "7. New Delhi",
                    IconSource = ImageSource.FromFile("New Delhi.png"),
                    websiteDescription = "The most populated city.",
                    url = "https://www.tripadvisor.com/Tourism-g304551-New_Delhi_National_Capital_Territory_of_Delhi-Vacations.html",
                },

                new WebsiteItem()
                {
                   websiteName = "8. Sydney",
                    IconSource = ImageSource.FromFile("Sydney.png"),
                    websiteDescription = "Discover interesting stuff.",
                    url = "https://www.australia.com/en-us/places/sydney-and-surrounds/guide-to-sydney.html",
                },

                new WebsiteItem()
               {
                 websiteName = "9. Seychelles",
                 IconSource = ImageSource.FromFile("Seychelles.png"),
                  websiteDescription = "The island with wonderful beaches.",
                   url = "https://www.lonelyplanet.com/seychelles",
               },

                new WebsiteItem()
                {
                    websiteName = "10. Cairo",
                    IconSource = ImageSource.FromFile("Cairo.png"),
                    websiteDescription = "Wow! Explore amazing pyramids.",
                    url = "https://www.lonelyplanet.com/egypt",

                },


                new WebsiteItem()
                {
                    websiteName = "11. Maldives",
                    IconSource = ImageSource.FromFile("Maldives.png"),
                    websiteDescription = "Houses on water!!! Interesting.",
                    url = "https://visitmaldives.com",
                },



            };


            WebSiteLink.ItemsSource = SiteList;
        }

        //public ObservableCollection<WebsiteItem> SiteList { get; set; }


        void Handle_MoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var websiteitem = (WebsiteItem)menuItem.CommandParameter;
            Navigation.PushAsync(new TravelMoreInfo(websiteitem));
        }

        void Handle_Delete(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var websiteitem = (ObservableCollection<WebsiteItem>)WebSiteLink.ItemsSource;
            WebsiteItem web = (WebsiteItem)menuItem.CommandParameter;
            websiteitem.Remove(web);
           

        }
    }
}
