﻿using System;
using System.Collections.Generic;
using HW4_ListViews.ClassModels;
using Xamarin.Forms;

namespace HW4_ListViews
{
    public partial class TravelMoreInfo : ContentPage
    {
        string link;
        public TravelMoreInfo()
        {
            InitializeComponent();
        }

        public TravelMoreInfo(WebsiteItem websiteitem)
        {
            InitializeComponent();

            BindingContext = websiteitem;

            link = websiteitem.url;
        }

        void Handle_OpenWebsite(object sender, System.EventArgs e)
        {
            var uri = new Uri(link);
            Device.OpenUri(uri);
        }

    }
}
