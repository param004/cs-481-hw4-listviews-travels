﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace HW4_ListViews.ClassModels
{
    public class WebsiteItem
    {
        public ImageSource IconSource
        {
            get;
            set;
        }

        public string websiteName
        {
            get;
            set;
        }


        public string websiteDescription
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }

       
    }
}
